package com.example.tugas4;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    TextView result, solution;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        result = findViewById(R.id.result);
        solution = findViewById(R.id.solution);

        solution.setText(getIntent().getExtras().getString("solution"));
        result.setText(getIntent().getExtras().getString("result"));


    }
}